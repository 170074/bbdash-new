import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import TypeCharts from "../components/Types"


const SentimentPage = () => (
  <Layout>
    <SEO title="Home" />
    <TypeCharts />
  </Layout>
)

export default SentimentPage
