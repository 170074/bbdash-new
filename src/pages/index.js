import React from "react"
import AboutPage from "../components/About"
import AlgorithmPage from "../components/Algorithm"
import HomePage from "../components/Home"
import styled from 'styled-components'

import media from '../styles/media'
import Layout from "../components/layout"
import SEO from "../components/seo"
import DefinitionsPage from "../components/Definitions"

const PageDiv = styled.div`
  padding: 20px 20px;
  ${media.laptop`
    padding: 20px 60px;
  `}
`

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <HomePage />
    <PageDiv>
      <AboutPage />
      <AlgorithmPage />
      <DefinitionsPage />
    </PageDiv>
  </Layout>
)

export default IndexPage
