import React from "react"
import InteractionCharts from "../components/Interactions"

import Layout from "../components/layout"
import SEO from "../components/seo"


const SentimentPage = () => (
  <Layout>
    <SEO title="Home" />
    <InteractionCharts />
  </Layout>
)

export default SentimentPage
