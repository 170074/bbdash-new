import React from "react"

import Layout from "../components/layout"
import SentimentCharts from "../components/Sentiment"
import SEO from "../components/seo"


const SentimentPage = () => (
  <Layout>
    <SEO title="Home" />
    <SentimentCharts />
  </Layout>
)

export default SentimentPage
