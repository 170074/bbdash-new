import React from "react"
import DemographicsCharts from "../components/Demographics"

import Layout from "../components/layout"
import SEO from "../components/seo"


const SentimentPage = () => (
  <Layout>
    <SEO title="Home" />
    <DemographicsCharts />
  </Layout>
)

export default SentimentPage
