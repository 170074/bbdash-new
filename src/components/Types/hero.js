import React, { useEffect, useRef } from "react"
import styled from 'styled-components'
import media from '../../styles/media'
import { TweenMax, Power3 } from 'gsap'
import { HeadText } from "../../styles"
import * as Scroll from "react-scroll"
import { SplitChars, Tween } from 'react-gsap'

const HeroCon = styled.div`
  padding: 15vh 5vw;
  background: linear-gradient(to right, #4990b7, #8ac926);
  p {
    &.top {
      font-size: 12px;
      margin: 0;
    }
    font-size: 14px;
    line-height: 25px;
    font-weight: 300;
    margin-top: 40px;
  }

  .heading {
    display: flex;
  }

  ${media.laptop`
    padding: 10vh 8vw 15vh;
    display: grid;
    grid-template-columns: 2fr 0.5fr;
    background: linear-gradient(to right, #4990b7, #8ac926);
    p {
      opacity: 0;
      font-size: 22px;
      line-height: 30px;
      font-weight: 300;
      margin: 50px 0;
    }
    .follow {
      margin: 5vh 0;
      a {
        text-decoration: none;
        color: white;
        background-color: #e1306c;
        padding: 20px 40px;
      }
    }
  `}
`

const Hero = () => {
  const introRef = useRef(null)

  const Element = Scroll.Element;

  useEffect(() => {
    TweenMax.to(
      introRef.current, 1, {
        opacity: 1,
        y: -20,
        ease: Power3.easeOut,
        delay: .8
      }
    )
  }, [])

  return (
    <Element name="home">
      <HeroCon>
        <div className="text">
          <HeadText>
            <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)">
              <SplitChars
                wrapper={<div style={{ display: 'inline-block'}} />}
              >
                Types&nbsp;
              </SplitChars>
            </Tween>
          </HeadText>
          <HeadText>
            <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)" delay={0.2}>
              <SplitChars
                wrapper={<div style={{ display: 'inline-block'}} />}
              >
                of&nbsp;
              </SplitChars>
            </Tween>
          </HeadText>
          <HeadText>
            <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)" delay={0.28}>
              <SplitChars
                wrapper={<div style={{ display: 'inline-block'}} />}
              >
                Tweets
              </SplitChars>
            </Tween>
          </HeadText>
          <div className="intro">
            <Tween from={{ opacity: 0, y: 20 }} delay={0.02}>
              <div>
            <p ref={introRef}>How are users interacting within<br />
               different folksonomies on Twitter?</p></div>
              </Tween>
          </div>
        </div>
      </HeroCon>
    </Element>
  )
}

export default Hero
