import React, { useEffect, useRef } from "react"
import styled from 'styled-components'
import media from '../../styles/media'
import { TweenMax, Power3 } from 'gsap'
import { HeadText } from "../../styles"
import * as Scroll from "react-scroll"
import { SplitChars, Tween } from 'react-gsap'

const HeroCon = styled.div`
padding: 20vh 5vw 15vh;
background-color: rgb(255, 220, 128);
  opacity: 1;
  background-image: repeating-radial-gradient(circle at 0px 0px, transparent 0px, rgb(255, 220, 128) 37px), repeating-linear-gradient(rgba(252, 175, 69, 0.333), rgb(252, 175, 69));
  margin: 0 !important;
  p {
    &.top {
      font-size: 12px;
      margin: 0;
    }
    font-size: 14px;
    line-height: 25px;
    font-weight: 300;
    margin-top: 40px;
  }

  .heading {
    display: flex;
  }

  ${media.laptop`
    padding: 25vh 8vw 15vh;
    display: grid;
    grid-template-columns: 2fr 0.5fr;
    p {
      opacity: 0;
      font-size: 22px;
      line-height: 30px;
      font-weight: 300;
      margin: 50px 0;
    }
    .follow {
      margin: 5vh 0;
      a {
        text-decoration: none;
        color: white;
        background-color: #e1306c;
        padding: 20px 40px;
      }
    }
  `}
`

const Hero = () => {
  const introRef = useRef(null)

  const Element = Scroll.Element;

  useEffect(() => {
    TweenMax.to(
      introRef.current, 1, {
        opacity: 1,
        y: -20,
        ease: Power3.easeOut,
        delay: .8
      }
    )
  }, [])

  return (
    <Element name="home">
      <HeroCon>
        <div className="text">
          <HeadText>
            <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)">
              <SplitChars
                wrapper={<div style={{ display: 'inline-block'}} />}
              >
                Demographics&nbsp;
              </SplitChars>
            </Tween>
          </HeadText>
          <div className="intro">
            <Tween from={{ opacity: 0, y: 20 }} delay={0.02}>
              <div>
            <p ref={introRef}>A look into the people interacting <br />
              within different folksonomies</p></div>
              </Tween>
          </div>
        </div>
      </HeroCon>
    </Element>
  )
}

export default Hero
