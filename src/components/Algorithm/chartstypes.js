import React from "react"
import styled from 'styled-components'
import media from '../../styles/media'
import * as Scroll from "react-scroll"
import {Pie} from 'react-chartjs-2';

const ChartsTypesCon = styled.div`
  padding: 5vh 0vw;
  margin: 0 !important;
  p {
    &.top {
      font-size: 12px;
      margin: 0;
    }
    font-size: 14px;
    line-height: 25px;
    font-weight: 300;
    margin-top: 40px;
  }

  .heading {
    display: flex;
  }

  .chartDiv, .chartDivR {
    canvas {
      margin: auto;
    width: 200px !important;
    height: 200px !important;
    }
    margin: auto;
  }

.chartRight {
  margin-bottom: 0;
   display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 1fr 0.5fr;
}
.chartDivR {
  grid-column: 1;
  grid-row: 2/3;
}
.text2 {
  grid-column: 1;
  grid-row: 1/2;
}

.chartLeft {
  margin-bottom: 15vh;
}

  ${media.laptop`
    padding: 0vh 5vw 15vh;
    .chartLeft {
      display: grid;
      grid-template-columns: 2fr 1fr;
      margin: 5vh 0;
    }
    .chartRight {
      display: grid;
      grid-template-columns: 1fr 2fr;
      grid-template-rows: 1fr;
      margin: 5vh 0;
    }
.chartDivR {
  grid-column: 1;
  grid-row: 1;
}
.text2 {
  grid-column: 2;
  grid-row: 1;
}
    p {
      opacity: 1;
      font-size: 22px;
      line-height: 30px;
      font-weight: 300;
      margin: 50px 0;
    }
    h3 {
      font-size: 25px;
      color: #ff595e;
    }
    .chartDiv, .chartDivR {
    canvas {
      margin-top:40px;
    width: 300px !important;
    height: 300px !important;
    }
    }
    
  `}
`

const ChartsTypes = () => {
  const Element = Scroll.Element;

  var options = {
    legend: {
      position: "bottom",
      labels: {
        usePointStyle: true,
        fontSize: 10,
        padding	: 15,
    }
    },
    segmentShowStroke : false
  };

  const data = {
      labels: [
          'Mobile',
          'Desktop'
      ],
      datasets: [{
          data: [73.38, 25.42],
          backgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
          ],
          hoverBackgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
          ],
          borderWidth: 0
      }],
  };

  const data2 = {
      labels: [
          'Own',
          'Replies',
          'Shares'
      ],
      datasets: [{
          data: [55.70, 24.84, 19.46],
          backgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
          ],
          hoverBackgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
          ],
          borderWidth: 0
      }],
  };

  return (
    <Element name="home">
      <ChartsTypesCon>
        <p>
        <strong>
          User's Interactions
          <br/><br/>
          How are users accessing the Folksonomies on Twitter, and what are they posting?
        </strong>
        </p>
        <div className="chartLeft">
          <div>
            <h3>Device</h3>
            <p>According to Hootsuite “a social media management platform” (Simpson 2020), South Africa’s social media usage increased by 50% in the first two months after the lockdown was implemented, with Twitter’s usage increasing by 14.09%. When looking at device ownership of internet users aged between 16 and 64, 94% own mobile phones, compared to 76% who own laptops or desktop computers. The web traffic of mobile phones is 73.4% compared to only 24.5% for laptops and desktops. this shows that more users in South Africa are connecting to the internet, and by extension social media, on their mobile devices. Hootsuite reported that 96% of the users aged between 16 and 64 use social networking mobile applications. Twitter falls within this realm.</p>
          </div>
          <div className="chartDiv">
            <Pie
              data={data}
              width={200}
              height={200}
              options={options}
            />
          </div>
        </div>
        <div className="chartRight">
          <div className="chartDivR">
            <Pie
              data={data2}
              width={200}
              height={200}
              options={options}
            />
          </div>
          <div className="text2">
            <h3>Content Type</h3>
            <p>The majority of posts within my chosen folksonomies are users’ own posts. this means that these are the original thoughts all tweets of the users interacting with them these folksonomies (#lockdownSouthAfrica, #loneliness, #belonging). This could prove that the tweets being sent out are more personal and involve users sharing their own personal details and experiences. The second-highest type of post is replies. this indicates that conversations are forming around these folksonomies. use this could be replying to one another's posts in order to find out more information, offer support, or simply express that they can relate. Lastly, users are sharing one another's posts. This could be in order to draw attention to specific instances that are taking place, or as a way to express that one relates to what the original user was saying in their post.</p>
          </div>
        </div>
      </ChartsTypesCon>
    </Element>
  )
}

export default ChartsTypes
