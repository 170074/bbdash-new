import React, { useState } from "react"
import styled from 'styled-components'
import { HeadText } from "../../styles"
import media from '../../styles/media'
import * as Scroll from "react-scroll"
import { Reveal, Tween, SplitChars } from 'react-gsap'
import Select from 'react-select'
import {Pie, Bar} from 'react-chartjs-2';
import AniLink from "gatsby-plugin-transition-link/AniLink"

const options = [
  { value: 'lockdownSouthAfrica', label: '#lockdownSouthAfrica' },
  { value: 'loneliness', label: '#loneliness' },
  { value: 'belonging', label: '#belonging' },
];

const AboutCitiesCon = styled.div`
  margin: 20px 0 10vh;
  /* border-bottom: 2px solid #4990b7; */
  p, a {
    &.top {
      font-size: 12px;
      margin: 0;
    }
    text-decoration: none;
    color: #313335;
    font-size: 14px;
    line-height: 25px;
    font-weight: 300;
    margin-top: 40px;
  }
  code {
    color: #6a4c93;
  }

  ${media.laptop`
    padding: 10vh 5vw 15vh;
    p, a {
      &.top {
        font-size: 14px;
        text-transform: uppercase;
        margin: 0;
      }
      margin: 50px 0 0;
      font-size: 22px;
      line-height: 30px;
      font-weight: 300;
      max-width: 67vw;
    }
    .pie-explaination {
      display: grid;
      grid-template-columns: 1fr 1fr;
      canvas {
        width: 450px !important;
        height: 450px !important;
      }
    }
  `}
`

const Cities = () =>  {
  const Element = Scroll.Element;

  return (
    <Element name="aboput">
      <AboutCitiesCon id="about">
        <div className="text">
          <Reveal>
            <HeadText>
              <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)">
                <SplitChars
                  wrapper={<div style={{ display: 'inline-block'}} />}
                >
                  Demographics&nbsp;
                </SplitChars>
              </Tween>
            </HeadText>
          </Reveal>
          <Reveal>
            <Tween from={{ opacity: 0, y: 20 }} duration={1}>
              <p>Main South African cities where different folksonomies are used
              <br/><br/>
              An exploration of the genders, ages, cities and interests of the people who interacted within the folksonomies that I chose, and how they see themselves.<br/><br/>
              This will allow a better understanding into the types of people interacting in different Folksonomies, and why these Folksonomies may be relevant to them.
              </p>
            </Tween>
            <br/><br/>
            <AniLink to="/demographics" cover direction="left" bg="#ff595e">
              Find out more about  the users who interactedin different Folksonomies &rarr;
            </AniLink>
          </Reveal>
        </div>
      </AboutCitiesCon>
    </Element>
  )
}

export default Cities
