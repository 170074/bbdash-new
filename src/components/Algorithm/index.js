import React from "react"
import Sentiment from "./sentiment"
import ChartsInteractions from "./chartsinteractions"
import ChartsTypes from "./chartstypes"
import Cities from "./cities"
import Hashtags from "./hashtags"
import Demographics from './demo'
import Hero from "./hero"

const AlgorithmPage = () => (
  <>
  <Sentiment />
  <ChartsInteractions />
  <ChartsTypes />
  <Hero />
  <Cities />
  <Demographics/>
  <Hashtags />
  </>
)

export default AlgorithmPage
