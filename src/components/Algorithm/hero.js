import React from "react"
import styled from 'styled-components'
import { HeadText } from "../../styles"
import media from '../../styles/media'
import * as Scroll from "react-scroll"
import { Reveal, Tween, SplitChars } from 'react-gsap'

const AboutHeroCon = styled.div`
  padding: 15vh 0;
  /* border-top: 2px solid #4990b7;
  border-bottom: 2px solid #4990b7; */
  p {
    &.top {
      font-size: 12px;
      margin: 0;
    }
    font-size: 14px;
    line-height: 25px;
    font-weight: 300;
    margin-top: 40px;
  }
  code {
    color: #ff595e;
    font-size: 9px;
    strong {
    font-size: 11px;
    }
  }

  ${media.laptop`
    padding: 20vh 5vw;
    p {
      &.top {
        font-size: 14px;
        text-transform: uppercase;
        margin: 0;
      }
      margin: 50px 0 0;
      font-size: 22px;
      line-height: 30px;
      font-weight: 300;
    }
    code {
      color: #ff595e;
      font-size: 21px;
      strong {
      font-size: 45px;
      }
    }
  `}
`

const Hero = () =>  {
  const Element = Scroll.Element;

  return (
    <Element name="aboput">
      <AboutHeroCon id="about">
        <div className="text">
          <Reveal>
            <HeadText>
              <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)">
                <SplitChars
                  wrapper={<div style={{ display: 'inline-block'}} />}
                >
                  Follower&nbsp;
                </SplitChars>
              </Tween>
            </HeadText>
            <HeadText>
              <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)" delay={0.32}>
                <SplitChars
                  wrapper={<div style={{ display: 'inline-block'}} />}
                >
                  Algorithm
                </SplitChars>
              </Tween>
            </HeadText>
          </Reveal>
          <Reveal>
            <Tween from={{ opacity: 0, y: 20 }} duration={1}>
              <p>Belonging Bot provided me with insights with regards to how and why users visit a profile. The act of exploring another user’s page, and finding similar interests and experiences could be a factor in gaining a feeling of social connectedness. Maslow’s Third Need states that people will aim to form part of a group. One of the first steps to being included in a group is to share a similar interest. By having users click on Belonging Bot’s page, it can be assumed that they found a post interesting and relevant to themselves and chose to explore more.
              <br/><br/></p>
            </Tween>
            <br/><br/>
            <Tween from={{ opacity: 0, y: 20 }} duration={1} delay={.4}>
            <code>if &#40; <strong>Follower Count</strong> &#60; 30 &amp;&amp; <strong>Sentiment Score</strong> &#60; 0 &#41; &#123; <br/>
              &nbsp;&nbsp;&nbsp;&nbsp; Users are more likely to connect with Belonging Bot <br/>
              &#125;
            </code>
            </Tween>
            <Tween from={{ opacity: 0, y: 20 }} duration={1} delay={.8}>
              <p>When analysing the tweets that Belonging Bot interacted with, accounts with more friends than followers often posted more tweets with a positive sentiment than negative. This could be as a result of the user feeling a sense of social connectedness, as they have found a community with which to interact on Twitter. The creations and interaction within this community satisfy Maslow’s third need, as interpersonal connections have been established. Interestingly, the majority of Belonging Bot’s followers were users who had posted tweets with negative sentiments and had Belonging Bot add them to a collection. This could show that users posting tweets with negative sentiments could be attempting to foster connections online and be aiming to form part of a community.</p>
            </Tween>
            <br/><br/>
          </Reveal>
        </div>
      </AboutHeroCon>
    </Element>
  )
}

export default Hero
