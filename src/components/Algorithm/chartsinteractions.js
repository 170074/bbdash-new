import React from "react"
import styled from 'styled-components'
import media from '../../styles/media'
import * as Scroll from "react-scroll"
import {Bar} from 'react-chartjs-2';

const ChartsInteractionsCon = styled.div`
  padding: 5vh 0vw;
  margin: 0 !important;
  p {
    &.top {
      font-size: 12px;
      margin: 0;
    }
    font-size: 14px;
    line-height: 25px;
    font-weight: 300;
    margin-top: 40px;
  }

  .heading {
    display: flex;
  }

  .chartDiv, .chartDivR {
    canvas {
      margin: auto;
      margin-top: 20px;
    width: 300px !important;
    height: 300px !important;
    }
    margin: auto;
  }

.chartRight {
  margin-bottom: 0;
   display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 1fr 0.5fr;
}
.chartDivR {
  grid-column: 1;
  grid-row: 2/3;
}
.text2 {
  grid-column: 1;
  grid-row: 1/2;
}

.chartLeft {
  margin-bottom: 15vh;
}

  ${media.laptop`
    padding: 0vh 5vw 15vh;
    display: block;
    .chartLeft {
      display: grid;
      grid-template-columns: 2fr 1fr;
      margin: 5vh 0;
    }
    .chartRight {
      display: grid;
      grid-template-columns: 1fr 2fr;
      grid-template-rows: 1fr;
      margin: 5vh 0;
    }
.chartDivR {
  grid-column: 1;
  grid-row: 1;
}
.text2 {
  grid-column: 2;
  grid-row: 1;
}
    p {
      opacity: 1;
      font-size: 22px;
      line-height: 30px;
      font-weight: 300;
      margin: 50px 0;
    }
    h3 {
      font-size: 25px;
      color: #ff595e;
    }
    .chartDiv, .chartDivR {
    canvas {
      margin-top:40px;
    width: 300px !important;
    height: 300px !important;
    }
    margin: auto;
  }
    
  `}
`

const ChartsInteractions = () => {
  const Element = Scroll.Element;

  var options = {
    legend: {
      position: "bottom",
      labels: {
        usePointStyle: true,
        fontSize: 10,
        padding	: 15,
    }
    },
    segmentShowStroke : false
  };

  const data = {
    labels: ['2020-09-11', '2020-09-12', '2020-09-13', '2020-09-14', '2020-09-15', '2020-09-16', '2020-09-17', '2020-09-18', '2020-09-19', '2020-09-20', '2020-09-21', '2020-09-22', '2020-09-23', '2020-09-24', '2020-09-25', '2020-09-26', '2020-09-27', '2020-09-28', '2020-09-29', '2020-09-30', '2020-10-01', '2020-10-02', '2020-10-03', '2020-10-04', '2020-10-05', '2020-10-06', '2020-10-07', '2020-10-08' ],
    datasets: [
      {
        label: 'Impressions',
        backgroundColor: '#6a4c93',
        borderColor: '#6a4c93',
        borderWidth: 1,
        hoverBackgroundColor: '#6a4c93',
        hoverBorderColor: '#6a4c93',
        data: [0, 0, 0, 0, 0, 0, 70, 1, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 4, 0, 1, 17, 2, 0, 54, 118, 25]
      }
    ]
};

const data2 = {
    labels: ['2020-09-11', '2020-09-12', '2020-09-13', '2020-09-14', '2020-09-15', '2020-09-16', '2020-09-17', '2020-09-18', '2020-09-19', '2020-09-20', '2020-09-21', '2020-09-22', '2020-09-23', '2020-09-24', '2020-09-25', '2020-09-26', '2020-09-27', '2020-09-28', '2020-09-29', '2020-09-30', '2020-10-01', '2020-10-02', '2020-10-03', '2020-10-04', '2020-10-05', '2020-10-06', '2020-10-07', '2020-10-08' ],
    datasets: [
      {
        label: 'Engagements',
        backgroundColor: '#4990b7',
        borderColor: '#4990b7',
        borderWidth: 1,
        hoverBackgroundColor: '#4990b7',
        hoverBorderColor: '#4990b7',
        data: [0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 24, 2]
      }
    ]
};

  return (
    <Element name="home">
      <ChartsInteractionsCon>
      <p>
      <strong>
        Types of Interactions
        <br/><br/>
        How are users interacting within different folksonomies on Twitter?
      </strong>
      </p>
        <div className="chartLeft">
          <div className="text1">
            <h3>Belonging Bot's Interactions</h3>
            <p>The act of exploring another user’s page, and finding similar interests and experiences could be a factor in gaining a feeling of social connectedness. Maslow’s Third Need states that people will aim to form part of a group. One of the first steps to being included in a group is to share a similar interest. By having users click on Belonging Bot’s page, it can be assumed that they found a post interesting and relevant to themselves and chose to explore more. Throughout this practical project, I have found that Belonging Bot received more impressions when its own, original content was posted, as opposed to when it reposted others’ tweets.</p>
          </div>
          <div className="chartDiv">
            <Bar
              data={data}
              width={200}
              height={200}
              options={options}
            />
          </div>
        </div>
        <div className="chartRight">
          <div className="chartDivR">
            <Bar
              data={data2}
              width={200}
              height={200}
              options={options}
            />
          </div>
          <div className="text2">
            <h3>Belonging Bot's Engagements</h3>
            <p>Belonging Bot gained more followers when it began posting statuses of its own, as opposed to just sharing other users’ posts. This is evident with an increase of 829% profile visits in the 28 days since Belonging Bot began posting statuses of its own. These statuses consisted of extracts from my research question, quotes from sources that I researched, and emoticons such as hearts to attempt to brighten up Belonging Bot’s profile and allow it to seem welcoming. Another insight of social connectedness on Twitter that I was able to draw from Belonging Bot’s interactions is that reaching out to other users will often result in the user reciprocating the effort.</p>
          </div>
        </div>
      </ChartsInteractionsCon>
    </Element>
  )
}

export default ChartsInteractions
