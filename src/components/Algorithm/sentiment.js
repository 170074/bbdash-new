import React, { useState } from "react"
import styled from 'styled-components'
import { HeadText } from "../../styles"
import media from '../../styles/media'
import * as Scroll from "react-scroll"
import { Reveal, Tween, SplitChars } from 'react-gsap'
import Select from 'react-select'
import {Pie, Bar} from 'react-chartjs-2';

const options = [
  { value: 'All', label: 'All Interactions' },
  { value: 'lockdownSouthAfrica', label: '#lockdownSouthAfrica' },
  { value: 'loneliness', label: '#loneliness' },
  { value: 'belonging', label: '#belonging' },
];

const AboutSentimentCon = styled.div`
  margin: 20px 0 10vh;
  /* border-bottom: 2px solid #4990b7; */
  p {
    &.top {
      font-size: 12px;
      margin: 0;
    }
    font-size: 14px;
    line-height: 25px;
    font-weight: 300;
    margin-top: 40px;
  }
  code {
    color: #6a4c93;
  }
    .pie-explaination {
      
      /* grid-template-columns: 1fr 1fr; */
      justify-content: space-between;
      canvas {
        margin: auto;
        margin-top: 40px;
        width: 200px !important;
        height: 200px !important;
      }
    }

  ${media.laptop`
    padding: 10vh 5vw 15vh;
    p {
      &.top {
        font-size: 14px;
        text-transform: uppercase;
        margin: 0;
      }
      margin: 80px 50px 0 0px;
      font-size: 22px;
      line-height: 30px;
      font-weight: 300;
      max-width: 67vw;
    }
    .pie-explaination {
      display: flex;
      /* grid-template-columns: 1fr 1fr; */
      justify-content: space-between;
      canvas {
        margin: 20px 0 0;
        width: 350px !important;
        height: 350px !important;
      }
    }
  `}
`

const Sentiment = () =>  {
  const Element = Scroll.Element;
  const [tag, setTag] = useState('All')

  const handleChange = selectedOption => {
    setTag(selectedOption.value)
  };

  var options2 = {
    legend: {
      position: "bottom",
      labels: {
        usePointStyle: true,
        fontSize: 10,
        padding	: 15,
    }
    },
    segmentShowStroke : false
  };

  const data = {
      labels: [
          'Terrible',
          'Bad',
          'Neutral',
          'Good',
          'Great'
      ],
      datasets: [{
          data: [1, 11, 61, 19, 8],
          backgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
              "#ffca3a",
              "#8ac926",
              "#8ac926"
          ],
          hoverBackgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
              "#ffca3a",
              "#8ac926",
              "#8ac926"
          ],
          borderWidth: 0
      }],
  };

  const data2 = {
      labels: [
          'Terrible',
          'Bad',
          'Neutral',
          'Good',
          'Great'
      ],
      datasets: [{
          data: [28.1, 27, 19.1, 7.9, 18],
          backgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
              "#ffca3a",
              "#8ac926",
              "#8ac926"
          ],
          hoverBackgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
              "#ffca3a",
              "#8ac926",
              "#8ac926"
          ],
          borderWidth: 0
      }]
  };

  const data3 = {
      labels: [
          'Terrible',
          'Bad',
          'Neutral',
          'Good',
          'Great'
      ],
      datasets: [{
          data: [9.1, 10.2, 59.1, 15.9, 5.7],
          backgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
              "#ffca3a",
              "#8ac926",
              "#8ac926"
          ],
          hoverBackgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
              "#ffca3a",
              "#8ac926",
              "#8ac926"
          ],
          borderWidth: 0
      }]
  };

  const data4 = {
      labels: [
          'Negative',
          'Neutral',
          'Positive'
      ],
      datasets: [{
          data: [152, 403, 625],
          backgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
          ],
          hoverBackgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
          ],
          borderWidth: 0
      }]
  };

  return (
    <Element name="aboput">
      <AboutSentimentCon id="about">
        <div className="text">
          <Reveal>
            <HeadText>
              <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)">
                <SplitChars
                  wrapper={<div style={{ display: 'inline-block'}} />}
                >
                  Belonging&nbsp;
                </SplitChars>
              </Tween>
            </HeadText>
            <HeadText>
              <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)" delay={0.36}>
                <SplitChars
                  wrapper={<div style={{ display: 'inline-block'}} />}
                >
                  Bot's&nbsp;
                </SplitChars>
              </Tween>
            </HeadText>
            <HeadText>
              <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)" delay={0.56}>
                <SplitChars
                  wrapper={<div style={{ display: 'inline-block'}} />}
                >
                  Findings
                </SplitChars>
              </Tween>
            </HeadText>
          </Reveal>
          <Reveal>
            <Tween from={{ opacity: 0, y: 20 }} duration={1}>
              <p><strong>
                Sentiment Analysis
                <br/><br/>
                Which sentiments are most common for the chosen folksonomies?
                </strong>
              </p>
            </Tween>
            <br/>
            <Select
              value={tag}
              onChange={selectedOption => handleChange(selectedOption)}
              options={options}
              placeholder={tag}
              theme={(theme) => ({
                ...theme,
                borderRadius: 0,
                colors: {
                ...theme.colors,
                  text: '#6a4c93',
                  neutral0: 'white',
                  primary25: '#6a4c93',
                  primary: '#6a4c93',
                },
              })}
            />
            <br/>
            {/* <p style={{textAlign: 'center'}}>#{tag}</p> */}
            {tag === 'All' && (
              <div className="pie-explaination">
                <p>Because Belonging Bot is aiming to create a positive space on Twitter, the majority of the tweets that it has interacted with and collected are positive. Neutral tweets have been collected, as it is often news sources sharing these, and negative tweets have been interacted with, as Belonging Bot aims to further understand social connectedness and, in doing so, has found that the users posing negative tweets are often aiming to find a sense of social connectedness online.
                </p>
                <div className="cityPie" >
                  <Pie
                    data={data}
                    width={200}
                    height={200}
                    options={options2}
                  />
                </div>
              </div>
            )}
            {tag === 'lockdownSouthAfrica' && (
              <div className="pie-explaination">
                <p>Tweets that contained the hashtag #lockdownSouthAfrica were predominantly neutral. This could be due to the fact that many of these tweets were shared by news outlets in order to inform citizens of what level South Africa was on at the time, as well as the restrictions that came with that level. A lot of the information that was shared within this folksonomy was factual and informative rather than emotional.
                </p>
                <div className="cityPie" >
                  <Pie
                    data={data2}
                    width={200}
                    height={200}
                    options={options2}
                  />
                </div>
              </div>
            )}
            {tag === 'loneliness' && (
              <div className="pie-explaination">
                <p>More than half of the tweets that were shared within this folksonomy had negative sentiments. This could be due to the fact that loneliness is usually associated with feeling sad or unsupported by the people around you. Users who share tweets with a negative sentiment within this folksonomy could be aiming to develop a sense of social connectedness online in order to alleviate those feelings.
                </p>
                <div className="cityPie" >
                  <Pie
                    data={data3}
                    width={200}
                    height={200}
                    options={options2}
                  />
                </div>
              </div>
            )}
            {tag === 'belonging' && (
              <div className="pie-explaination">
                <p>Tweets that contained the hashtag #belonging contained the most varied sentiments of the folksonomies that I explored. The majority of them were neutral, which suggests that they were either informative, or also aiming to gain insight on how people are coping with the societal lockdown. Positive sentiments were the second largest group within this folksonomy, which could allude to the fact that people are creating and experiencing a sense of belonging on Twitter. The negative sentiments expressed were mostly from users who were not experiencing a sense of belonging, and expressing their disappointment.
                </p>
                <div className="cityPie" >
                  <Pie
                    data={data4}
                    width={200}
                    height={200}
                    options={options2}
                  />
                </div>
              </div>
            )}
          </Reveal>
        </div>
      </AboutSentimentCon>
    </Element>
  )
}

export default Sentiment
