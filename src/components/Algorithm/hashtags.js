import React, { useState } from "react"
import styled from 'styled-components'
import { HeadText } from "../../styles"
import media from '../../styles/media'
import * as Scroll from "react-scroll"
import { Reveal, Tween, SplitChars } from 'react-gsap'
import Select from 'react-select'
import TagCloud from 'react-tag-cloud'

const options = [
  { value: 'lockdownSouthAfrica', label: '#lockdownSouthAfrica' },
  { value: 'loneliness', label: '#loneliness' },
  { value: 'belonging', label: '#belonging' },
];

const AboutHashtagsCon = styled.div`
  padding: 20px 0;
  /* border-bottom: 2px solid #4990b7; */
  p {
    &.top {
      font-size: 12px;
      margin: 0;
    }
    font-size: 14px;
    line-height: 25px;
    font-weight: 300;
    margin-top: 40px;
  }
  code {
    color: #6a4c93;
  }

  ${media.laptop`
    padding: 20vh 5vw 15vh;
    p {
      &.top {
        font-size: 14px;
        text-transform: uppercase;
        margin: 0;
      }
      margin: 50px 0 0;
      font-size: 22px;
      line-height: 30px;
      font-weight: 300;
      text-align: center;
      &.intro {
        text-align: left;
      }
    }
    .pie-explaination {
      display: grid;
      grid-template-columns: 1fr 1fr;
      canvas {
        width: 450px !important;
        height: 450px !important;
      }
    }
  `}
`

const Hashtags = () =>  {
  const Element = Scroll.Element;
  const [tag, setTag] = useState('lockdownSouthAfrica')

  const handleChange = selectedOption => {
    setTag(selectedOption.value)
  };

  return (
    <Element name="aboput">
      <AboutHashtagsCon id="about">
        <div className="text">
          <Reveal>
            <HeadText>
              <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)">
                <SplitChars
                  wrapper={<div style={{ display: 'inline-block'}} />}
                >
                  Popular&nbsp;
                </SplitChars>
              </Tween>
            </HeadText>
            <HeadText>
              <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)" delay={0.28}>
                <SplitChars
                  wrapper={<div style={{ display: 'inline-block'}} />}
                >
                  Hashtags
                </SplitChars>
              </Tween>
            </HeadText>
          </Reveal>
          <Reveal>
            <Tween from={{ opacity: 0, y: 20 }} duration={1}>
              <p className="intro">Hashtags associated wiith different Folksonomies
              <br/><br/></p>
            </Tween>
            <br/><br/>
            <Select
              value={tag}
              onChange={selectedOption => handleChange(selectedOption)}
              options={options}
              placeholder={tag}
              theme={(theme) => ({
                ...theme,
                borderRadius: 0,
                colors: {
                ...theme.colors,
                  text: '#6a4c93',
                  neutral0: 'white',
                  primary25: '#6a4c93',
                  primary: '#6a4c93',
                },
              })}
            />
            <br/><br/>
            <p>#{tag}</p>
            {tag === 'lockdownSouthAfrica' && (
              <>
                <div className="pie-explaination">
                  <TagCloud
                    style={{
                      fontFamily: 'sans-serif',
                      fontSize: 30,
                      fontWeight: 'bold',
                      padding: 5,
                      width: '87vw',
                      height: '400px'
                    }}>
                    <div style={{fontSize: 64}}>#NewProfilePic</div>
                    <div style={{fontSize: 80, color: '#4990b7'}}>#COVID19</div>
                    <div style={{fontSize: 46}}>#zamisto</div>
                    <div style={{fontSize: 36}}>#veganmuffin</div>
                    <div style={{fontSize: 28}}>#vegan</div>
                    <div style={{fontSize: 29}}>#summermenu</div>
                    <div style={{fontSize: 72}}>#staysafe</div>
                    <div style={{fontSize: 68}}>#stayhome</div>
                    <div style={{fontSize: 48}}>#smooth</div>
                    <div style={{fontSize: 55}}>#saota</div>
                  </TagCloud>
                </div>
                <br/><br/>
                <p>When looking at the hashtags that are associated with the folksonomy #lockdownSouthAfrica, we can see that #COVID19 is the most used, more popular one. This is because the lockdown was only implemented due to the pandemic. Another two hashtags that were very popular within this folksonomy are #staysafe and #stayhome. These were initiatives that were launched in order to get people to abide by the regulations set out in the lockdown. They show that users care about their fellow citizens, and are aiming to stop the spread of the disease. </p>
              </>
            )}
            {tag === 'loneliness' && (
              <>
                <div className="pie-explaination">
                  <TagCloud 
                    style={{
                      fontFamily: 'sans-serif',
                      fontSize: 30,
                      fontWeight: 'bold',
                      padding: 5,
                      width: '87vw',
                      height: '400px'
                    }}>
                    <div style={{fontSize: 64}}>#Senekal</div>
                    <div style={{fontSize: 58}}>#youpromisedtomarryme</div>
                    <div style={{fontSize: 76}}>#worldmentalhealthday</div>
                    <div style={{fontSize: 79, color: '#4990b7'}}>#speakoutagainstracism</div>
                    <div style={{fontSize: 59}}>#orlandopirates</div>
                    <div style={{fontSize: 78}}>#hatespeech</div>
                    <div style={{fontSize: 52}}>#countrydut</div>
                    <div style={{fontSize: 67}}>#ZonkeMchunu</div>
                  </TagCloud>
                </div>
                <br/><br/>
                <p>The hashtags most commonly associated with the folksonomy #loneliness centre around putting an end to racism and hate speech. This can be seen with hashtags such as #speakoutagainstracism and #hatespeech. Being discriminated against because of the colour of your skin is a harrowing experience that can lead to feelings of loneliness. Oher hashtags associated with this folksonomy centre around  television entertainment. This could be because people are spending more time doing these activities alone because of the pandemic. </p>
              </>
            )}
            {tag === 'belonging' && (
              <>
                <div className="pie-explaination">
                  <TagCloud 
                    style={{
                      fontFamily: 'sans-serif',
                      fontSize: 30,
                      fontWeight: 'bold',
                      padding: 5,
                      width: '87vw',
                      height: '400px'
                    }}>
                    <div style={{fontSize: 64, color: '#4990b7'}}>#told</div>
                    <div style={{fontSize: 11}}>#mistake</div>
                    <div style={{fontSize: 16}}>#thought</div>
                    <div style={{fontSize: 17}}>#bad</div>
                  </TagCloud>
                </div>
                <br/><br/>
                <p>The hashtags that were most popular within the folksonomy #belonging did not create a cohesive story in the way that the other folksonomies seemed to do. This could be because belonging is so different to each individual person, and the thoughts expressed on Twitter around the subject do not follow any specific pattern. Hashtags such as #told and #thought show that users may be sharing their own experiences and stories around the topic of belonging. </p>
              </>
            )}
          </Reveal>
        </div>
      </AboutHashtagsCon>
    </Element>
  )
}

export default Hashtags
