import React from "react"
import styled from 'styled-components'

import media from '../../styles/media'
import Hero from "./hero"
import ChartsInteractions from "./chartsinteractions"

const PageDiv = styled.div`
  padding: 20px 20px;
  ${media.laptop`
    padding: 20px 60px;
  `}
`

const InteractionCharts = () => (
  <>
  <Hero />
  <PageDiv>
    <ChartsInteractions />
  </PageDiv>
  </>
)

export default InteractionCharts
