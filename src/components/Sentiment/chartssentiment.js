import React from "react"
import styled from 'styled-components'
import media from '../../styles/media'
import * as Scroll from "react-scroll"
import {Pie} from 'react-chartjs-2';

const ChartsSentimentCon = styled.div`
  padding: 5vh 0vw;
  margin: 0 !important;
  p {
    &.top {
      font-size: 12px;
      margin: 0;
    }
    font-size: 14px;
    line-height: 25px;
    font-weight: 300;
    margin-top: 40px;
  }

  .heading {
    display: flex;
  }

  .chartDiv {
    canvas {
    width: 200px !important;
    height: 200px !important;
    }
    margin: auto;
  }

  .chartLeft {
    margin: 15vh 0 0 0;
  }
  .chartRight {
    margin: 15vh 0 0 0;
  }

  ${media.laptop`
    padding: 10vh 5vw 15vh;
    .chartLeft {
      display: grid;
      grid-template-columns: 2fr 1fr;
      margin: 5vh 0;
    }
    .chartRight {
      display: grid;
      grid-template-columns: 1fr 2fr;
      margin: 5vh 0;
    }
    p {
      opacity: 1;
      font-size: 22px;
      line-height: 30px;
      font-weight: 300;
      margin: 50px 0;
    }
    h3 {
      font-size: 25px;
      color: #ff595e;
    }
    
  `}
`

const ChartsSentiment = () => {
  const Element = Scroll.Element;

  var options = {
    legend: {
      position: "bottom",
      labels: {
        usePointStyle: true,
        fontSize: 10,
        padding	: 15,
    }
    },
    segmentShowStroke : false
  };

  const data = {
      labels: [
          'Terrible',
          'Bad',
          'Neutral',
          'Good',
          'Great'
      ],
      datasets: [{
          data: [1, 11, 61, 19, 8],
          backgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
              "#ffca3a",
              "#8ac926",
              "#8ac926"
          ],
          hoverBackgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
              "#ffca3a",
              "#8ac926",
              "#8ac926"
          ],
          borderWidth: 0
      }],
  };

  const data2 = {
      labels: [
          'Terrible',
          'Bad',
          'Neutral',
          'Good',
          'Great'
      ],
      datasets: [{
          data: [28.1, 27, 19.1, 7.9, 18],
          backgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
              "#ffca3a",
              "#8ac926",
              "#8ac926"
          ],
          hoverBackgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
              "#ffca3a",
              "#8ac926",
              "#8ac926"
          ],
          borderWidth: 0
      }]
  };

  const data3 = {
      labels: [
          'Terrible',
          'Bad',
          'Neutral',
          'Good',
          'Great'
      ],
      datasets: [{
          data: [9.1, 10.2, 59.1, 15.9, 5.7],
          backgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
              "#ffca3a",
              "#8ac926",
              "#8ac926"
          ],
          hoverBackgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
              "#ffca3a",
              "#8ac926",
              "#8ac926"
          ],
          borderWidth: 0
      }]
  };

  const data4 = {
      labels: [
          'Negative',
          'Neutral',
          'Positive'
      ],
      datasets: [{
          data: [152, 403, 625],
          backgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
          ],
          hoverBackgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
          ],
          borderWidth: 0
      }]
  };

  return (
    <Element name="home">
      <ChartsSentimentCon>
          <p>Sentiment analysis is a way to treat “opinions, sentiments and subjectivity of text” through computational processes (Medha, Hassan &amp; Korashy 2014:1). Thus, it is a way to discover people's opinions towards subject-matter online.</p>
          <br/><br/>
          <div className="chartLeft">
            <div>
              <h3>Overall sentiment of collected Tweets</h3>
              <p>Because Belonging Bot is aiming to create a positive space on Twitter, the majority of the tweets that it has interacted with and collected are positive. Neutral tweets have been collected, as it is often news sources sharing these, and negative tweets have been interacted with, as Belonging Bot aims to further understand social connectedness and, in doing so, has found that the users posing negative tweets are often aiming to find a sense of social connectedness online.</p>
            </div>
            <div className="chartDiv">
              <Pie
                data={data4}
                width={200}
                height={200}
                options={options}
              />
            </div>
          </div>
          <div className="chartRight">
            <div className="chartDiv">
              <Pie
                data={data}
                width={200}
                height={200}
                options={options}
              />
            </div>
            <div>
              <h3>#lockdownSouthAfrica</h3>
              <p>Tweets that contained the hashtag #lockdownSouthAfrica were predominantly neutral. This could be due to the fact that many of these tweets were shared by news outlets in order to inform citizens of what level South Africa was on at the time, as well as the restrictions that came with that level. A lot of the information that was shared within this folksonomy was factual and informative rather than emotional.</p>
            </div>
          </div>
          <div className="chartLeft">
            <div>
              <h3>#loneliness</h3>
              <p>More than half of the tweets that were shared within this folksonomy had negative sentiments. This could be due to the fact that loneliness is usually associated with feeling sad or unsupported by the people around you. Users who share tweets with a negative sentiment within this folksonomy could be aiming to develop a sense of social connectedness online in order to alleviate those feelings.</p>
            </div>
            <div className="chartDiv">
              <Pie
                data={data2}
                width={200}
                height={200}
                options={options}
              />
            </div>
          </div>
          <div className="chartRight">
            <div className="chartDiv">
              <Pie
                data={data3}
                width={200}
                height={200}
                options={options}
              />
            </div>
            <div>
              <h3>#belonging</h3>
              <p>Tweets that contained the hashtag #belonging contained the most varied sentiments of the folksonomies that I explored. The majority of them were neutral, which suggests that they were either informative, or also aiming to gain insight on how people are coping with the societal lockdown. Positive sentiments were the second largest group within this folksonomy, which could allude to the fact that people are creating and experiencing a sense of belonging on Twitter. The negative sentiments expressed were mostly from users who were not experiencing a sense of belonging, and expressing their disappointment.</p>
            </div>
          </div>
      </ChartsSentimentCon>
    </Element>
  )
}

export default ChartsSentiment
