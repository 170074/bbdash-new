import React from "react"
import styled from 'styled-components'

import media from '../../styles/media'
import Hero from "./hero"
import ChartsSentiment from "./chartssentiment"

const PageDiv = styled.div`
  padding: 20px 20px;
  ${media.laptop`
    padding: 20px 60px;
  `}
`

const SentimentCharts = () => (
  <>
  <Hero />
  <PageDiv>
    <ChartsSentiment />
  </PageDiv>
  </>
)

export default SentimentCharts
