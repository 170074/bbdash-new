import React, { useEffect, createRef } from "react"
import styled from 'styled-components'
import { HeadText } from "../../styles"
import media from '../../styles/media'
import * as Scroll from "react-scroll"
import { Reveal, Tween, SplitChars } from 'react-gsap'
import lottie from 'lottie-web'

import animation from '../../images/who.json'

const AboutHeroCon = styled.div`
  padding: 20px 0 10vh;
  /* border-top: 2px solid #4990b7;
  border-bottom: 2px solid #4990b7; */
  p {
    &.top {
      font-size: 12px;
      margin: 0;
    }
    font-size: 14px;
    line-height: 25px;
    font-weight: 300;
    margin-top: 40px;
  }
  .animation-container {
    /* display: none; */
    width: 35vw;
    margin: 0 auto;
  }

  ${media.laptop`
    padding: 20vh 5vw 20vh;
    .who {
      display: grid;
      grid-template-columns: 0.5fr 1fr;
    }
    p {
      &.top {
        font-size: 14px;
        text-transform: uppercase;
        margin: 0;
      }
      margin: 50px 0 0;
      font-size: 22px;
      line-height: 30px;
      font-weight: 300;
    }
    .animation-container {
      width: auto;
      display: block;
      margin: 50px;
    }
  `}
`

const Hero = () =>  {
  const Element = Scroll.Element;
  let animationContainer = createRef();

  useEffect(() => {
    const anim = lottie.loadAnimation({
      container: animationContainer.current,
      renderer: "svg",
      loop: true,
      autoplay: true,
      animationData: animation
    });
    return () => anim.destroy(); // optional clean up for unmounting
  }, []);

  return (
    <Element name="aboput">
      <AboutHeroCon id="about">
        <div className="text">
          <Reveal>
            <HeadText>
              <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)">
                <SplitChars
                  wrapper={<div style={{ display: 'inline-block'}} />}
                >
                  Who&nbsp;
                </SplitChars>
              </Tween>
            </HeadText>
            <HeadText>
              <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)" delay={0.12}>
                <SplitChars
                  wrapper={<div style={{ display: 'inline-block'}} />}
                >
                  is&nbsp;
                </SplitChars>
              </Tween>
            </HeadText>
            <HeadText>
              <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)" delay={0.20}>
                <SplitChars
                  wrapper={<div style={{ display: 'inline-block'}} />}
                >
                  Belonging&nbsp;
                </SplitChars>
              </Tween>
            </HeadText>
            <HeadText>
              <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)" delay={0.56}>
                <SplitChars
                  wrapper={<div style={{ display: 'inline-block'}} />}
                >
                  Bot?&nbsp;
                </SplitChars>
              </Tween>
            </HeadText>
          </Reveal>
          <div className="who">
            <div className="animation-container" ref={animationContainer} />
            <Reveal>
              <Tween from={{ opacity: 0, y: 20 }} duration={1} delay={.2}>
                <p>Belonging Bot is a Twitter Bot that interacts with other Twitter users that are discussing topics such as: Loneliness, Belonging and the Lockdown in South Africa. It does this to try to understand social connectedness online further, by analysing Twitter interactions with the help of Folksonomies and Sentiment Analysis!
                <br/><br/>
                Once it understands the experiences of different communities in South Africa during this time, my bot will try to understand how and why conversations between users areperpetuated. Belonging Bot attempts to encourage the sharing of positive posts by responding to and retweeting them - adding imagery and emojis to include a visual break from all of the text that Twitter is usually made up of. </p>
              </Tween>
            </Reveal>
          </div>
        </div>
      </AboutHeroCon>
    </Element>
  )
}

export default Hero
