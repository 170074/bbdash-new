import React, { useEffect, createRef } from "react"
import styled from 'styled-components'
import media from '../../styles/media'
import {Pie, Bar} from 'react-chartjs-2';
import { HeadText } from "../../styles"
import { Reveal, Tween, SplitChars } from 'react-gsap'
import AniLink from "gatsby-plugin-transition-link/AniLink"
import lottie from 'lottie-web'

import animation from '../../images/findings.json'

const AboutChartsCon = styled.div`
  padding: 20px 0;
  p {
    &.top {
      font-size: 12px;
      margin: 0;
    }
    font-size: 14px;
    line-height: 25px;
    font-weight: 300;
    margin-top: 40px;
    span {
      font-size: 12px;
      color: #ff595e;
    }
  }
  a {
      padding: 50px 20px;
      color: #313335;
      text-decoration: none;
  }
  .chartDiv {
    canvas {
    width: 200px !important;
    height: 200px !important;
    margin-left: 30vw;
    }
  }
  .animation-container {
    display: none;
  }

  ${media.laptop`
    padding: 15vh 0;
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-template-rows: 1fr 1fr;
    gap: 30px 30px;
    p {
      &.top {
        font-size: 14px;
        text-transform: uppercase;
        margin: 0;
      }
      margin: 50px 0 0;
      font-size: 22px;
      line-height: 30px;
      font-weight: 300;
      max-width: 67vw;
      span {
        font-size: 14px;
      }
    }

    a {
      padding: 50px 20px;
      border: 1px solid rgba(255, 220, 128, 0.5);
      color: #313335;
      text-decoration: none;
      .chartDiv {
        canvas {
          width: 250px !important;
          height: 250px !important;
          float: right;
        }
      }
    }
    .text {
      margin: 10vh 0;
    }
    
  .chartDiv {
    canvas {
    margin-left: 0vw;
    }
  }
  .animation-container {
      display: block;
      width: 25vw;
      margin: auto;
    }
  `}
`

const ChartsPreview = () =>  {
  let animationContainer = createRef();

  useEffect(() => {
    const anim = lottie.loadAnimation({
      container: animationContainer.current,
      renderer: "svg",
      loop: true,
      autoplay: true,
      animationData: animation
    });
    return () => anim.destroy(); // optional clean up for unmounting
  }, []);

  var options = {
    legend: {
        position: "bottom",
        labels: {
            usePointStyle: true,
            fontSize: 20,
            padding	: 20,
        }
    },
    segmentShowStroke : false
  };

  const data4 = {
      labels: [
          'Negative',
          'Neutral',
          'Positive'
      ],
      datasets: [{
          data: [152, 403, 625],
          backgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
          ],
          hoverBackgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
          ],
          borderWidth: 0
      }],
  }

  const barData = {
      labels: ['#lockdownSouthAfrica', '#loneliness', '#belonging'],
      datasets: [
        {
          label: 'Male',
          backgroundColor: '#6a4c93',
          borderColor: '#6a4c93',
          borderWidth: 1,
          hoverBackgroundColor: '#6a4c93',
          hoverBorderColor: '#6a4c93',
          data: [44.32, 44.32, 38.22]
        }, {
        label: 'Female',
        backgroundColor: '#4990b7',
        borderColor: '#4990b7',
        borderWidth: 1,
        hoverBackgroundColor: '#4990b7',
        hoverBorderColor: '#4990b7',
        data: [55.68, 55.68, 61.78],
        borderWidth: 0
      },
      ]
  };

  const barData2 = {
      labels: ['Own', 'Replies', 'Shares'],
      datasets: [
        {
          label: 'Amount of Content',
          borderWidth: 1,
          data: [55.7, 24.84, 19.46],
          backgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
          ],
          hoverBackgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
          ],
          borderWidth: 0
        },
      ]
  };

  const pieData2 = {
      labels: [
          'Engagements',
          'Impressions'
      ],
      datasets: [{
          data: [311, 458],
          backgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
          ],
          hoverBackgroundColor: [
            "#6a4c93",
            "#4990b7",
            "#ff595e",
          ],
          borderWidth: 0
      }],
  }

  return (
    <AboutChartsCon id="about">
       <div className="text">
          <Reveal>
            <HeadText>
              <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)">
                <SplitChars
                  wrapper={<div style={{ display: 'inline-block'}} />}
                >
                  Belonging&nbsp;
                </SplitChars>
              </Tween>
            </HeadText>
            <HeadText>
              <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)" delay={0.36}>
                <SplitChars
                  wrapper={<div style={{ display: 'inline-block'}} />}
                >
                  Bot's&nbsp;
                </SplitChars>
              </Tween>
            </HeadText>
            <HeadText>
              <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)" delay={0.56}>
                <SplitChars
                  wrapper={<div style={{ display: 'inline-block'}} />}
                >
                  Findings
                </SplitChars>
              </Tween>
            </HeadText>
        </Reveal>
      </div>
      <AniLink to="/sentiment" cover direction="left" bg="#ff595e">
        <Reveal>
          <Tween from={{ opacity: 0, y: 20 }} duration={1}>
            <div className="chartDiv">
              <p><strong>Sentiment Analysis</strong><br/> <span>Find out more &rarr;</span></p>
              <Pie
                data={data4}
                width={200}
                height={200}
                options={options}
              />
            </div>
          </Tween>
        </Reveal>
      </AniLink>
      <AniLink to="/demographics" cover direction="right" bg="#ff595e">
        <Reveal>
          <Tween from={{ opacity: 0, y: 20 }} duration={1}>
            <div className="chartDiv">
              <p><strong>Demographics</strong><br/> <span>Find out more &rarr;</span></p>
              <Bar
                data={barData}
                width={200}
                height={200}
                options={options}
              />
            </div>
          </Tween>
        </Reveal>
      </AniLink>
      <AniLink to="/types" cover direction="left" bg="#ff595e">
        <Reveal>
          <Tween from={{ opacity: 0, y: 20 }} duration={1}>
            <div className="chartDiv">
              <p><strong>Types of content</strong><br/> <span>Find out more &rarr;</span></p>
              <Bar
                data={barData2}
                width={200}
                height={200}
                options={options}
              />
            </div>
          </Tween>
        </Reveal>
      </AniLink>
      <AniLink to="/interactions" cover direction="right" bg="#ff595e">
        <Reveal>
          <Tween from={{ opacity: 0, y: 20 }} duration={1}>
            <div className="chartDiv">
              <p><strong>Interactions</strong><br/> <span>Find out more &rarr;</span></p>
                <Pie
                  data={pieData2}
                  width={200}
                  height={200}
                  options={options}
                />
            </div>
          </Tween>
        </Reveal>
      </AniLink>
      <div className="animation-container" ref={animationContainer} />
    </AboutChartsCon>
  )
}

export default ChartsPreview
