import React, { useEffect, createRef } from "react"
import styled from 'styled-components'
import { HeadText } from "../../styles"
import media from '../../styles/media'
import * as Scroll from "react-scroll"
import { Reveal, Tween, SplitChars } from 'react-gsap'
import lottie from 'lottie-web'

import animation from '../../images/where.json'

const AboutNextCon = styled.div`
  margin: 20px 0;
  p {
    &.top {
      font-size: 12px;
      margin: 0;
    }
    font-size: 14px;
    line-height: 25px;
    font-weight: 300;
    margin-top: 40px;
  }
  .animation-container {
    display: none;
  }
  code {
    color: #ff595e;
  }

  ${media.laptop`
    margin: 20vh 5vw;
    .who {
      display: grid;
      grid-template-columns: 1fr 0.6fr ;
    }
    p {
      &.top {
        font-size: 14px;
        text-transform: uppercase;
        margin: 0;
      }
      margin: 50px 0 0;
      font-size: 22px;
      line-height: 30px;
      font-weight: 300;
    }
    .animation-container {
      display: flex;
      margin: 50px;
    }
  `}
`

const Next = () =>  {
  const Element = Scroll.Element;
  let animationContainer = createRef();

  useEffect(() => {
    const anim = lottie.loadAnimation({
      container: animationContainer.current,
      renderer: "svg",
      loop: true,
      autoplay: true,
      animationData: animation
    });
    return () => anim.destroy(); // optional clean up for unmounting
  }, []);

  return (
    <Element name="aboput">
      <AboutNextCon id="about">
        <div className="text">
          <Reveal>
            <HeadText>
              <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)">
                <SplitChars
                  wrapper={<div style={{ display: 'inline-block'}} />}
                >
                  What's&nbsp;
                </SplitChars>
              </Tween>
            </HeadText>
            <HeadText>
              <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)" delay={0.24}>
                <SplitChars
                  wrapper={<div style={{ display: 'inline-block'}} />}
                >
                  Next?
                </SplitChars>
              </Tween>
            </HeadText>
          </Reveal>
          <div className="who">
            <Reveal>
              <Tween from={{ opacity: 0, y: 20 }} duration={1}>
                <p>With regards to future research, this study can be built upon to better incorporate social media users from different locations and within different folksonomies into online communities, by fostering a sense of social connectedness. Through this research, I discovered which users are reaching out in search of social connectedness in the form of conversations online. I aim to encourage online spaces to become more accepting of these users, and to better integrate them into online communities. Discovering specific folksonomies, as well as different geographic locations, in which users feel isolated or lonely could be done through surveys and interacting with more users. These users could come from a wider range of folksonomies, geographical locations, and social media platforms. By discovering the shortcomings within these spaces, social connectedness could be better fostered online and, by interacting within different folksonomies, and on different platforms, I would be able to see if the trends that I identified correspond.
                <br/><br/></p>
              </Tween>
            </Reveal>
            <div className="animation-container" ref={animationContainer} />
          </div>
        </div>
      </AboutNextCon>
    </Element>
  )
}

export default Next
