import React from "react"
import styled from 'styled-components'
import { HeadText } from "../../styles"
import media from '../../styles/media'
import * as Scroll from "react-scroll"
import { Reveal, Tween, SplitChars } from 'react-gsap'

const DefHeroCon = styled.div`
  padding: 20px 0;
  /* border-top: 2px solid #4990b7;
  border-bottom: 2px solid #4990b7; */
  p {
    &.top {
      font-size: 12px;
      margin: 0;
    }
    font-size: 14px;
    line-height: 25px;
    font-weight: 300;
    margin-top: 40px;
  }

  .animation-container {
    display: none;
  }

  ${media.laptop`
    padding: 20vh 5vw 20vh;
    .who {
      display: grid;
      grid-template-columns: 1fr 0.6fr;
    }
    p {
      &.top {
        font-size: 14px;
        text-transform: uppercase;
        margin: 0;
      }
      margin: 50px 0 0;
      font-size: 22px;
      line-height: 30px;
      font-weight: 300;
    }

    .animation-container {
      display: flex;
      margin: 50px;
    }
  `}
`

const Hero = () =>  {
  const Element = Scroll.Element;

  return (
    <Element name="definitions">
      <DefHeroCon id="definitions">
        <div className="text">
          <Reveal>
            <HeadText>
              <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)">
                <SplitChars
                  wrapper={<div style={{ display: 'inline-block'}} />}
                >
                  Definitions
                </SplitChars>
              </Tween>
            </HeadText>
          </Reveal>
          <Reveal>
            <Tween from={{ opacity: 0, y: 20 }} duration={1} delay={.2}>
              <p>
              Sentiment Analysis
              <br/><br/>
              Sentiment analysis is a way to treat “opinions, sentiments and subjectivity of text” through computational processes (Medha, Hassan &amp; Korashy 2014:1). Thus, it is a way to discover people's opinions towards subject-matter online.
            </p>
            <p>
              Folksonomies
              <br/><br/>
              A folksonomy is a way in which items on the internet are described and categorised by users (Neal 2007:7). This is done by attaching keywords to posts through the use of tags and hashtags, to structure and organise them.
            </p>
            <p>
              Social Connectedness
              <br/><br/>
              In the article, “How Social Are Social Media? A Review of Online Social Behaviour and Connectedness”(2017) by Tracii Ryan, Kelly A. Allen, DeLeon L. Gray and Dennis M. McInerney, social connectedness is defined as feeling as though one belongs in the same group as, and relates to, one’s peers (2017:1).
            </p>
            </Tween>
          </Reveal>
        </div>
      </DefHeroCon>
    </Element>
  )
}

export default Hero
