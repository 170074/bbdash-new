import React from "react"
import Hero from "./hero"
import Next from "./next"

const DefinitionsPage = () => (
  <>
  <Hero />
  <Next />
  </>
)

export default DefinitionsPage
