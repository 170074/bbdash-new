import React from "react"
import styled from 'styled-components'
import media from '../styles/media'
import * as Scroll from "react-scroll"
import AniLink from "gatsby-plugin-transition-link/AniLink"

const HeaderCon = styled.header`
  height: 5vh;
  width: 90vw;
  background-color: transparent;
  position: absolute;
  display: flex;
  justify-content: space-between;
  .home {
    font-size: 22px;
    margin: 5px 0;
    color: #ff595e;
    cursor: pointer;
  } a {
      font-size: 16px;
      margin: 5px 0;
      color: #ff595e;
      text-decoration: none;
      font-family: 'lust-didone';
    }
  .menu-items {
    display: flex;
    margin: 0;
    a {
      font-weight: 100;
      font-size: 12px;
      cursor: pointer;
        font-family: "ff-tisa-sans-web-pro";
        color: #4e4e4e;
      :nth-child(1) {
        margin-right: 20px;
      }
    }
  }

  padding: 20px 20px;

  ${media.laptop`
    padding: 20px 60px;
    width: 90vw;
    height: 35px;
    .home {
      font-size: 25px;
    }
    a {
      font-size: 22px;
      margin: 5px 0;
      color: #ff595e;
      text-decoration: none;
      font-family: 'lust-didone';
    }
    .menu-items {
      display: flex;
      margin: 10px 0 0;
      a {
        font-size: 14px;
        text-decoration: none;
        display: inline-block;
        position: relative;
        opacity: 1;
        font-family: "ff-tisa-sans-web-pro";
        color: #4e4e4e;
        cursor: pointer;
        :nth-child(1) {
          margin-right: 50px;
        }
        ::before {
          transition: 300ms;
          height: 2px;
          content: "";
          position: absolute;
          background-color: #4e4e4e;
          width: 0%;
          bottom: -5px;
        }

        :hover::before {
          width: 100%;
        }
      }
    }
  `}
`

const Header = () => {
  const Link = Scroll.Link;

  return (
    <HeaderCon>
      <AniLink to="/" cover direction="down" bg="#ff595e">Belonging Bot</AniLink>
      <div className="menu-items">
        <Link to="about" spy={true} smooth={true} offset={-50} duration={700}>About</Link>
        <Link to="definitions" spy={true} smooth={true} offset={-50} duration={700}>Definitions</Link>
      </div>
    </HeaderCon>
  )
}

export default Header
