import React from "react"
import styled from 'styled-components'
import media from '../../styles/media'
import { HeadText } from "../../styles"
import { Reveal, Tween, SplitChars } from 'react-gsap'
import { TwitterTimelineEmbed } from 'react-twitter-embed';

const TweetsCon = styled.div`
  display: block;
  padding: 15vh 5vw 0vh;
  h1 {
    margin-bottom: 10vh;
  }
  .text {
    margin: 10vh 0;
    font-size: 14px;
    line-height: 25px;
    font-weight: 300;
    margin-top: 5vh;
      p {
        margin: 5vh 0;
        border-bottom: 1px solid #ffca3a;
      }
  }

  ${media.laptop`
  .stats {
    display: grid;
    grid-template-columns: 1fr 1fr;
    gap: 20px 20px;
    margin-top: 5vh;
  }
    padding: 20vh 10vw 5vh;
    .text {
      margin: auto 0 auto 5vw;
      font-size: 22px;
      line-height: 30px;
      font-weight: 300;
      max-width: 67vw;
      p {
        margin: 10vh 0;
        border-bottom: 1px solid #ffca3a;
      }
    }
  `}
`

const Tweets = () => {

  return (
    <TweetsCon>
      <Reveal>
        <HeadText>
          <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)">
            <SplitChars
              wrapper={<div style={{ display: 'inline-block'}} />}
            >
              Twitter&nbsp;
            </SplitChars>
          </Tween>
        </HeadText>
        <HeadText>
          <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)" delay={0.28}>
            <SplitChars
              wrapper={<div style={{ display: 'inline-block'}} />}
            >
              Statistics
            </SplitChars>
          </Tween>
        </HeadText>
      </Reveal>
      <div className="stats">
        <TwitterTimelineEmbed
          sourceType="profile"
          screenName="BelongingBot"
          options={{height: 500}}
        />

        <div className="text">
          <Reveal>
            <Tween from={{ opacity: 0, y: 20 }} duration={1}>
              <p>
              21 Followers
              </p>
            </Tween>
          </Reveal>
          <Reveal>
            <Tween from={{ opacity: 0, y: 20 }} duration={1}>
              <p>
              101 Following
              </p>
            </Tween>
          </Reveal>
          <Reveal>
            <Tween from={{ opacity: 0, y: 20 }} duration={1}>
              <p>
              192 Tweets
              </p>
            </Tween>
          </Reveal>
          <Reveal>
            <Tween from={{ opacity: 0, y: 20 }} duration={1}>
              <p>
              265 Likes
              </p>
            </Tween>
          </Reveal>
        </div>
      </div>
    </TweetsCon>
  )
}

export default Tweets
