import React, { useEffect, useRef, createRef } from "react"
import styled from 'styled-components'
import media from '../../styles/media'
import { TweenMax, Power3 } from 'gsap'
import { HeadText } from "../../styles"
import * as Scroll from "react-scroll"
import { SplitChars, Tween } from 'react-gsap'
import lottie from 'lottie-web'

import animation from '../../images/typing2.json'

const HeroCon = styled.div`
  padding: 25vh 5vw 0vh;
  height: 80vh;
  background-color: rgb(255, 220, 128);
  opacity: 1;
  background-image: repeating-radial-gradient(circle at 0px 0px, transparent 0px, rgb(255, 220, 128) 37px), repeating-linear-gradient(rgba(252, 175, 69, 0.333), rgb(252, 175, 69));
  p {
    &.top {
      font-size: 12px;
      margin: 0;
    }
    font-size: 14px;
    line-height: 25px;
    font-weight: 300;
    margin-top: 40px;
    text-align: center;
  }
  .follow {
    margin: 5vh 17vw 2vh;
    a {
      text-decoration: none;
      color: white;
      background-color: #ff595e;
      padding: 10px 20px;
    }
  }

  .vid {
    width: 30vw;
    margin: 0 28vw 5vh;
  }

  .heading {
    margin: 0 18vw;
  }

  ${media.laptop`
    padding: 30vh 8vw 0;
    display: grid;
    grid-template-columns: 1.2fr 1fr;
    .text {
      grid-row: 1;
      grid-column: 1;
    }
    p {
      text-align: left;
      opacity: 0;
      font-size: 22px;
      line-height: 30px;
      font-weight: 300;
      margin: 50px 0;
    }
    .follow {
      margin: 5vh 0;
      a {
        text-decoration: none;
        color: white;
        background-color: #ff595e;
        padding: 20px 40px;
      }
    }
    .vid {
      grid-column: 2;
      grid-row: 1;
      width: 320px;
      height: 320px;
      overflow: hidden;
      margin: 0 auto;
      video {
        width: 200%;
        margin: 0;
        margin-left: -55%;
      }
    }
    .heading {
      margin: 0;
    }
  `}
`

const Hero = () => {
  const introRef = useRef(null)
  let animationContainer = createRef();

  const Element = Scroll.Element;

  useEffect(() => {
    TweenMax.to(
      introRef.current, 1, {
        opacity: 1,
        y: -20,
        ease: Power3.easeOut,
        delay: .8
      }
    )
  }, [])

  useEffect(() => {
    const anim = lottie.loadAnimation({
      container: animationContainer.current,
      renderer: "svg",
      loop: true,
      autoplay: true,
      animationData: animation
    });
    return () => anim.destroy(); // optional clean up for unmounting
  }, []);

  return (
    <Element name="home">
      <HeroCon>
        <div className="vid">
          <div className="animation-container" ref={animationContainer} />
        </div>
        <div className="text">
          <div className="heading">
          <HeadText>
            <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)">
              <SplitChars
                wrapper={<div style={{ display: 'inline-block'}} />}
              >
                Belonging&nbsp;
              </SplitChars>
            </Tween>
          </HeadText>
          <HeadText>
            <Tween from={{ opacity: 0, y: 20 }} stagger={0.04} ease="elastic.out(0.1, 0.1)" delay={0.36}>
              <SplitChars
                wrapper={<div style={{ display: 'inline-block'}} />}
              >
                Bot
              </SplitChars>
            </Tween>
          </HeadText>
          </div>
          <div className="intro">
            <Tween from={{ opacity: 0, y: 20 }} delay={0.04}>
              <div>
            <p ref={introRef}>A means to understand social connectedness online <br />
              through folksonomies &amp; sentiment analysis on Twitter.</p></div>
              </Tween>
            <Tween from={{ opacity: 0, y: 20 }} delay={0.08}>
              <div className="follow">
              <a className="button"
                href="https://twitter.com/BelongingBot"
                target="_blank"
                data-size="large">
                  Follow @BelongingBot
              </a>
              </div>
              </Tween>
          </div>
        </div>
      </HeroCon>
    </Element>
  )
}

export default Hero

