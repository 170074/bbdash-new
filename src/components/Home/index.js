import React from "react"
import Tweets from "./tweets"
import Hero from "./hero"

const HomePage = () => (
  <>
  <Hero />
  <Tweets />
  </>
)

export default HomePage
