import styled from 'styled-components'
import media from './media'

// HEADER
export const HeadText = styled.h1`
  font-size: 26px;
  line-height: 44px;
  color: #ff595e;
  padding: 0;
  margin: 0;
  display: inline-block;
  text-align: center;

  ${media.laptop`
    font-size: 70px;
    line-height: 88px;
    text-align: left;
  `}
`